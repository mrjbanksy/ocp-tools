[![Docker Repository on Quay](https://quay.io/repository/mrjbanksy/ocp-tools/status "Docker Repository on Quay")](https://quay.io/repository/mrjbanksy/ocp-tools)

# ocp-tools

## What is ocp-tools?

`ocp-tools` is a container image based on the [Red Hat ubi9/OpenJDK 21 builder image](https://catalog.redhat.com/software/containers/ubi9/openjdk-21/6501cdb5c34ae048c44f7814) that has the `oc` and `helm` clients added that are useful when compiling projects and interacting with OpenShift within a GitLab pipeline.

## How do I use ocp-tools?

Specify it as the image to use in your pipeline by adding the following line:
```
image: quay.io/mrjbanksy/ocp-tools:latest
```

## How do I report issues?

Please open an issue at [https://gitlab.com/mrjbanksy/ocp-tools/-/issues](https://gitlab.com/mrjbanksy/ocp-tools/-/issues)